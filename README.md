**Red Hat Fuse Integration Platform 7.9.0 Docker Image**

This is a Docker image which allows to run the Fuse Integration Platform 7.9.0
on top of EAP 7.4.0.

Red Hat Fuse Integration Platform can be ran in several ways:

1. As an assembly deployed on EAP 7.4.0
2. As an assembly deployed on Karaf 4.3.x
3. As an OpenShift service
4. As a set of libraries on Spring Boot

While all the previous mentioned deployment models have their respective 
advantages, running Fuse Integration as a standalone assembly, aka deployed on 
either EAP or Karaf, is one of the preferred topology for organizations 
having specific security requirements, preventing them from an ad-hoc cloud adoption.

But running Fuse Integration in a standalone way requires a quite complex 
infrastructure, consisting in an existent EAP or Karaf platform. Here we're 
considering running Fuse Integration on EAP because Karaf, as an OSGi 
implemntation, becomes a bit less attractive nowadays. So, in order to get a
running Fuse Integration installation, the following steps are required:

1. Install EAP 7.4.0 (we recall that EAP, aka Enterprise Application Platform, is the current name of JBoss).
2. Apply all the required patches and security advises.
3. Configure the EAP installation with the required users, JDBC drivers, data-sources, etc.
4. Install the Fuse Integration assembly on the existent EAP instance.

Beside being complex, these operations are time-consuming and fastidious, 
especially when they need to be repeated often. Hence the interest of a Docker 
image able to provide out-of-the-box a full running and already configured Fuse
Integration platform.

---

## How to use the image

In order to use this Docker image you need to perform the following operations:

```
$ mkdir tests
$ cd tests
$ git clone https://nicolasduminil2@bitbucket.org/nicolasduminil2/red-hat-fuse-integration-platform-7.9.0.git
$ cd red-hat-fuse-integration-platform-7.9.0/
$ mvn -Pdocker install
```

The operations sequence above is cloning the Bitbucket repository containing 
the required artifacts to build the Docker image. Then running the maven command
will build the new image and will instantiate a Docker container, based on this 
new image, to run an EAP 7.4.0 server with the Fuse Integration 7.9.0 on the 
top of it. It will also configure the resulting platform with ad administrator
user as well as with an Oracle data-source and the required JDBC driver.
A separate Docker container running an Oracle 11 XE image will be instantiated 
as well.

## How to check the environment

The execution of these commands may take sometime. Once finished, we can check 
this way the execution success:

```
$ docker images
REPOSITORY                        TAG                      IMAGE ID            CREATED             SIZE
jboss-fuse-eap-7.9.0              1.0-SNAPSHOT             aec10cc7f8cb        3 days ago          790MB
openjdk                           11.0.7-jdk-slim-buster   85229f071913        17 months ago       402MB
oracleinanutshell/oracle-xe-11g   latest                   ad13c30ec346        2 years ago         2.13GB
$ docker ps 
CONTAINER ID        IMAGE                                    COMMAND                  CREATED             STATUS                    NAMES
595adc624f9c        jboss-fuse-eap-7.9.0:1.0-SNAPSHOT        "/bin/sh -c '/docker…"   3 days ago          Exited (137) 3 days ago   jboss-fuse
361ac7905f68        oracleinanutshell/oracle-xe-11g:latest   "/bin/sh -c '/usr/sb…"   3 days ago          Exited (137) 3 days ago   oracle
```

Before continuing, you need to make sure that the Fuse Integration platform has 
successfully started. Use the command below:

```
$ docker logs jboss-fuse --details --follow
...
 >>> JBoss Fuse 7.9.0 has been installed on EAP 7.4.0
 >>> Restart JBoss EAP Fuse Integration Server ...
 JAVA_OPTS already set in environment; overriding default settings with values: -Doracle.jdbc.timezoneAsRegion=false -Djboss.server.default.config=standalone.xml -Djboss.http.port=8080 -Djboss.management.http.port=9990 -Dapp.context.root=test -Djboss.bind.address=0.0.0.0 -Djboss.bind.address.management=0.0.0.0
 =========================================================================
 
   JBoss Bootstrap Environment
 
   JBOSS_HOME: /usr/lib/jboss-fuse
 
   JAVA: /usr/local/openjdk-11/bin/java
 
   JAVA_OPTS:  -server -Xlog:gc*:file="/usr/lib/jboss-fuse/standalone/log/gc.log":time,uptimemillis:filecount=5,filesize=3M -Doracle.jdbc.timezoneAsRegion=false -Djboss.server.default.config=standalone.xml -Djboss.http.port=8080 -Djboss.management.http.port=9990 -Dapp.context.root=test -Djboss.bind.address=0.0.0.0 -Djboss.bind.address.management=0.0.0.0  --add-exports=java.base/sun.nio.ch=ALL-UNNAMED --add-exports=jdk.unsupported/sun.misc=ALL-UNNAMED --add-exports=jdk.unsupported/sun.reflect=ALL-UNNAMED
 
 =========================================================================
...
12:27:38,480 INFO  [org.jboss.as.connector.subsystems.datasources] (ServerService Thread Pool -- 42) WFLYJCA0004: Deploying JDBC-compliant driver class oracle.jdbc.OracleDriver (version 19.3)
 12:27:38,497 INFO  [org.jboss.as.connector.deployers.jdbc] (MSC service thread 1-1) WFLYJCA0018: Started Driver service with driver-name = oracle
 12:27:38,524 INFO  [org.jboss.as.ejb3] (MSC service thread 1-6) WFLYEJB0481: Strict pool slsb-strict-max-pool is using a max instance size of 192 (per class), which is derived from thread worker pool sizing.
 12:27:38,524 INFO  [org.jboss.as.ejb3] (MSC service thread 1-5) WFLYEJB0482: Strict pool mdb-strict-max-pool is using a max instance size of 48 (per class), which is derived from the number of CPUs on this host.
 12:27:38,535 INFO  [org.wildfly.extension.undertow] (ServerService Thread Pool -- 70) WFLYUT0014: Creating file handler for path '/usr/lib/jboss-fuse/welcome-content' with options [directory-listing: 'false', follow-symlink: 'false', case-sensitive: 'true', safe-symlink-paths: '[]']
 12:27:38,541 INFO  [org.wildfly.extension.undertow] (MSC service thread 1-5) WFLYUT0012: Started server default-server.
 12:27:38,543 INFO  [org.wildfly.extension.undertow] (MSC service thread 1-1) Queuing requests.
 12:27:38,546 INFO  [org.wildfly.extension.undertow] (MSC service thread 1-1) WFLYUT0018: Host default-host starting
 12:27:38,602 INFO  [org.wildfly.extension.undertow] (MSC service thread 1-5) WFLYUT0006: Undertow HTTP listener default listening on 0.0.0.0:8080
 12:27:38,650 INFO  [org.jboss.as.ejb3] (MSC service thread 1-7) WFLYEJB0493: Jakarta Enterprise Beans subsystem suspension complete
 12:27:38,668 INFO  [org.jboss.as.patching] (MSC service thread 1-1) WFLYPAT0050: JBoss EAP cumulative patch ID is: jboss-eap-7.4.1.CP, one-off patches include: none
 12:27:38,674 INFO  [org.jboss.as.connector.subsystems.datasources.AbstractDataSourceService$AS7DataSourceDeployer] (MSC service thread 1-7) IJ020018: Enabling <validate-on-match> for java:jboss/datasources/OracleDS
 12:27:38,682 WARN  [org.jboss.as.domain.management.security] (MSC service thread 1-2) WFLYDM0111: Keystore /usr/lib/jboss-fuse/standalone/configuration/application.keystore not found, it will be auto generated on first use with a self signed certificate for host localhost
 12:27:38,688 INFO  [org.jboss.as.server.deployment.scanner] (MSC service thread 1-6) WFLYDS0013: Started FileSystemDeploymentService for directory /usr/lib/jboss-fuse/standalone/deployments
 12:27:38,694 INFO  [org.jboss.as.connector.subsystems.datasources] (MSC service thread 1-3) WFLYJCA0001: Bound data source [java:jboss/datasources/OracleDS]
 12:27:38,695 INFO  [org.jboss.as.connector.subsystems.datasources] (MSC service thread 1-3) WFLYJCA0001: Bound data source [java:jboss/datasources/ExampleDS]
 12:27:38,695 INFO  [org.jboss.as.server.deployment] (MSC service thread 1-1) WFLYSRV0027: Starting deployment of "hawtio-wildfly-2.0.0.fuse-sb2-790047-redhat-00001.war" (runtime-name: "hawtio-wildfly-2.0.0.fuse-sb2-790047-redhat-00001.war")
 12:27:38,703 INFO  [org.wildfly.extension.undertow] (MSC service thread 1-6) WFLYUT0006: Undertow HTTPS listener https listening on 0.0.0.0:8443
 12:27:38,725 INFO  [org.wildfly.extension.camel] (MSC service thread 1-4) Bound camel naming object: java:jboss/camel/CamelContextRegistry
 12:27:38,735 INFO  [org.jboss.ws.common.management] (MSC service thread 1-5) JBWS022052: Starting JBossWS 5.4.2.Final-redhat-00001 (Apache CXF 3.3.9.redhat-00001) 
 12:27:40,107 INFO  [org.infinispan.CONTAINER] (ServerService Thread Pool -- 74) ISPN000128: Infinispan version: Infinispan 'Corona Extra' 11.0.12.Final-redhat-00001
 12:27:40,148 INFO  [org.infinispan.CONFIG] (MSC service thread 1-7) ISPN000152: Passivation configured without an eviction policy being selected. Only manually evicted entities will be passivated.
 12:27:40,150 INFO  [org.infinispan.CONFIG] (MSC service thread 1-7) ISPN000152: Passivation configured without an eviction policy being selected. Only manually evicted entities will be passivated.
 12:27:40,221 INFO  [org.infinispan.PERSISTENCE] (ServerService Thread Pool -- 74) ISPN000556: Starting user marshaller 'org.wildfly.clustering.infinispan.spi.marshalling.InfinispanProtoStreamMarshaller'
 12:27:40,376 INFO  [org.jboss.as.clustering.infinispan] (ServerService Thread Pool -- 74) WFLYCLINF0002: Started http-remoting-connector cache from ejb container
 12:27:40,460 INFO  [io.hawt.HawtioContextListener] (ServerService Thread Pool -- 81) Initialising hawtio services
 12:27:40,467 INFO  [io.hawt.system.ConfigManager] (ServerService Thread Pool -- 81) Configuration will be discovered via JNDI
 12:27:40,468 INFO  [io.hawt.jmx.JmxTreeWatcher] (ServerService Thread Pool -- 81) Welcome to hawtio 2.0.0.fuse-sb2-790047-redhat-00001
 12:27:40,491 INFO  [io.hawt.system.ProxyWhitelist] (ServerService Thread Pool -- 81) Probing local addresses ...
 12:27:40,492 INFO  [io.hawt.system.ProxyWhitelist] (ServerService Thread Pool -- 81) Initial proxy whitelist: [localhost, 127.0.0.1, 192.168.96.2, jboss-fuse]
 12:27:40,568 INFO  [org.wildfly.extension.undertow] (ServerService Thread Pool -- 81) WFLYUT0021: Registered web context: '/hawtio' for server 'default-server'
 12:27:40,569 INFO  [org.wildfly.extension.camel] (ServerService Thread Pool -- 81) Add Camel endpoint: http://0.0.0.0:8080/hawtio
 12:27:40,620 INFO  [org.jboss.as.server] (ServerService Thread Pool -- 43) WFLYSRV0010: Deployed "hawtio-wildfly-2.0.0.fuse-sb2-790047-redhat-00001.war" (runtime-name : "hawtio-wildfly-2.0.0.fuse-sb2-790047-redhat-00001.war")
 12:27:40,655 INFO  [org.jboss.as.server] (Controller Boot Thread) WFLYSRV0212: Resuming server
 12:27:40,658 INFO  [org.jboss.as] (Controller Boot Thread) WFLYSRV0025: JBoss EAP 7.4.1.GA (WildFly Core 15.0.4.Final-redhat-00001) started in 4802ms - Started 520 of 722 services (349 services are lazy, passive or on-demand)
 12:27:40,659 INFO  [org.jboss.as] (Controller Boot Thread) WFLYSRV0060: Http management interface listening on http://0.0.0.0:9990/management
 12:27:40,659 INFO  [org.jboss.as] (Controller Boot Thread) WFLYSRV0051: Admin console listening on http://0.0.0.0:9990
```

Running the previous command, your output has to be similar with the listing 
above.

To make sure that the EAP 7.4.0 has successfully been installed, go to 
http://<host>:9990, to display the JBoss console. Here <host> is the IP address
of the Docker container which may be obtained via the command docker inspect.

To make sure that the Fuse Integration assembly has successfully been deployed,
go to http://<host>:8080/hawtio.

The project also contains a simple Camel route for test purposes. Make sure it 
works as expected by doing the following command sequence:

```
$ cd fuse/simple-camel-route
$ mvn -Pdocker wildfly:deploy
```

Here you're using the wildfly maven plugin in order to deploy the Camel route
on the Fuse Integration platform. The EAP container will ask for authentication.
Use the credentials defined in the auto.xml file (by default nicolas/password1).

Once the deploy command has been executed, look in the container's log file and
make sure that everything is fine. You should see log messages showing that records
have been inserted into the Oracle database. If you have sqldeveloper, or any 
other Oracle SQL client, you may also connect to the database and to look into the
ORDERS table such that to confirm that records have been inserted. The Oracle
database default credentials are by default the same (nicolas/password1).

Enjoy !